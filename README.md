# BTSBOT - Chat Bot for Discord

Open source, free to use chat bot for discord.

A few of the features included in this bot:

- bot-wide statistics (`leaderboard`,`toptips`,`winners`)
- individual favorites list (`addfavorite`,`removefavorite`,`tipfavorites`,`favorites`)
- Administration commands for specific users or roles (see `adminhelp`)
- Interactive help for user friendly-ness

And much more than listed here probably

## About

Some highlights:

- Transactions are queued and processed synchronously in a worker thread, while bot activity is handled in a main thread.
- User data, transactions, and all other persisted data is stored using the Peewee ORM with Sqlite
- Operates with a single Bitshares wallet, with 1 account per user.

Recommend using with a GPU/OpenCL configured node (or work peer) on busier discord servers due to POW calculation.

## Getting Started

```
pip install -r requirements.txt
```

### Configuration

```
cp settings.py.example settings.py
```

Edit `settings.py`

### Database backups

There exists a script in scripts/cron called `nanotipbotbackup`. Highly recommend you use this or something better to backup the tip bot database.

Simply toss the script in cron.hourly, crontab, cron.daily, whatever - and update the backup path and database path.

### Running

```
python3 bot.py
```

or in background:

```
nohup python3 bot.py &
```



# Acknowledgements

* Development was funded by [ADSactly](http://adsactly.com)
* Modified from [NANO bot](https://github.com/bbedward/Graham_Nano_Tip_Bot)
* [Great tutorial](https://www.devdungeon.com/content/make-discord-bot-python)
