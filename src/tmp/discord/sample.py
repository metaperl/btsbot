# 3rd party
from configobj import ConfigObj
import discord
from discord.ext.commands import Bot



config = ConfigObj('system.ini')
TOKEN  = config['botuser']['token']

my_bot = Bot(command_prefix="!")


@my_bot.event
async def on_message(message):

    print("Message {}".format(message))

    # we do not want the bot to reply to itself
    if message.author == my_bot.user:
        return

    if message.content.startswith('!meme'):
        msg = 'Hello {0.author.mention}'.format(message)
        await my_bot.send_message(message.channel, msg)

@my_bot.event
async def on_ready():
    print('Logged in as')
    print(my_bot.user.name)
    print(my_bot.user.id)
    print('------')

my_bot.login(TOKEN)
my_bot.run(TOKEN)
