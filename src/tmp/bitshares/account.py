# Obtain Account balance, open orders and history
# http://docs.pybitshares.com/en/latest/index.html#quickstart

from bitshares.account import Account


account = Account("floatation7")

def echo_print(label, value):
    print("""
<{}>
{}
</{}>
""".format(label,value,label))

echo_print('balances', account.balances)
echo_print('openorders', account.openorders)
for h in account.history():
    echo_print('history', h)
