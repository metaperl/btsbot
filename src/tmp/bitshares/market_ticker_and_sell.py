# Print Market ticker and sell

from bitshares.market import Market

from echo_print import echo_print

def pmarket(base, quote='BTC'):
    market_label = '{}:{}'.format(base, quote)
    market = Market(market_label)
    echo_print('market_ticker', market.ticker())

pmarket('USD', 'BTS')
pmarket('BTS', 'USD')
pmarket('BTC', 'USD')
pmarket('ADXCU')
pmarket('ADXCU', 'BTS')


# market.bitshares.wallet.unlock("wallet-passphrase")
# print(market.sell(300, 100))  # sell 100 USD for 300 BTS/USD
