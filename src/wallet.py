import util
import db
import datetime
import settings
import asyncio
import aiohttp
import socket

wallet = settings.wallet

logger = util.get_logger('wallet')
logger_newuser = util.get_logger('usr', log_file='user_creation.log')


async def communicate_wallet_async(wallet_command):
    conn = aiohttp.TCPConnector(
        family=socket.AF_INET6,
        resolver=aiohttp.AsyncResolver())
    async with aiohttp.ClientSession(connector=conn) as session:
        async with session.post("http://[::1]:7076", json=wallet_command, timeout=300) as resp:
            return await resp.json()


async def get_balance(user, coin):
    user_id = user.user_id
    if user is None:
        logger.info('user %s does not exist.', user_id)
        return {'actual': 0,
                'available': 0,
                'pending_send': 0,
                'pending': 0}
    else:
        logger.info('Fetching %s balance from wallet for %s', coin, user_id)
        return db.get_user_balance(user_id, coin)


async def soft_send_coin(source_userid, amount, coin_id, uid, target_userid=None, giveaway_id=0, verify_address=False):
    # Do not validate address for giveaway tx because we do not know it yet
    if verify_address:
        # Check to see if the withdraw address is valid
        wallet_command = {'action': 'validate_account_number',
                          'account': withdraw_address}
        address_validation = await communicate_wallet_async(wallet_command)

        if (((withdraw_address[:4] == 'xrb_' and len(withdraw_address) != 64)
             or (withdraw_address[:5] == 'nano_' and len(withdraw_address) != 65))
                or address_validation['valid'] != '1'):
            raise util.TipBotException('invalid_address')

    # See if destination address belongs to a user
    if targetuser_id is None:
        user = db.get_user_by_wallet_address(withdraw_address)
        if user is not None:
            target_id = user.user_id
    # Update pending send for user
    db.create_transaction(
        source_userid,
        uid,
        coin_id,
        amount,
        target_userid,
        giveaway_id)
    logger.info('TX queued, uid %s', uid)

    return amount


async def make_transaction_to_user(user, amount, coin_id, target_user_id, target_user_name, uid):
    target_user = await db.create_or_fetch_user(target_user_id, target_user_name)
    try:
        actual_tip_amount = await soft_send_coin(user, amount, coin_id, uid, target_user_id)
    except util.TipBotException as e:
        return 0

    logger.info('tip queued. (from: %s, to: %s, amount: %d, uid: %s)',
                user.user_id, target_user.user_id, actual_tip_amount, uid)
    return actual_tip_amount
